FROM tensorflow/tensorflow:latest-gpu-py3

RUN apt-get update -qq 
RUN apt-get install -y git wget
RUN pip install --upgrade pip

RUN pip install --upgrade gensim
RUN pip install -U spacy
RUN pip install ufal.udpipe wget future pymystem3 jsonpickle matplotlib rhymex keras-bert
RUN pip install python-telegram-bot --upgrade

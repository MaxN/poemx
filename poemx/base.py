from .dry_nlp import load_model, index2word, load_token
from .dry_nlp_word import DryNlpWord
from .model import get_model

from pathlib import Path
from random import randint
from rhymex import is_rhyme
from rhymex.word_profile import WordProfile
import numpy as np

from .hyperparams import SEQ_LENGTH
import random
from .bert import is_sentence_follows

class Base:
  def __init__(self, model_path):
    print('SEQ_LENGTH', SEQ_LENGTH)
    self.dry_nlp = DryNlpWord(model_output='tmp', seq_length=SEQ_LENGTH)
    path =  str(Path(__file__).parent.resolve())
    # import pdb; pdb.set_trace()
    self.model = get_model(seq_length=SEQ_LENGTH - 1, words_num=self.dry_nlp.words_num)
    self.model.summary()
    self.model.load_weights(path+'/models/model_weights.h5')
    self.dry_nlp.token = load_token(path + "/models/token.json")
    

  def include(self, item, list):
    try:
      list.index(item)
      return True
    except:
      return False


  def calc_grade(self, line, stress_word):
    ret = 0
    first_word =WordProfile(line[0])
    # print("calc_grade: line={}, stress_word={}".format(line, stress_word))
    stress_word =WordProfile(stress_word)
    

    pw = None
    if first_word.syllables_count == stress_word.syllables_count:
      ret = ret + 200
    if first_word.stressed_syllable_index == stress_word.stressed_syllable_index:
      ret = ret + 100

    for w in line:
      wp = WordProfile(w)
      if first_word.syllables_count == wp.syllables_count:
        ret = ret + 10
      if wp.syllables_count >=2:
        ret = ret + 10 
      if first_word.stressed_syllable_index == wp.stressed_syllable_index:
        ret = ret + 10

      if w == pw:
        ret = ret - 100
      pw = w
    

    # buf = ""
    # for i in range(1, len(line) - 1):
    #   buf = " ".join(line[:i])
    #   # import pdb; pdb.set_trace()
    #   prob =  is_sentence_follows(buf, line[i])
    #   print("{} -> {}: {}%".format(buf, line[i], prob))
    #   ret += prob
    # print('calc_grade: {}'.format(ret/len(line)))
    return ret/len(line)

  def gen_first_word(self, start_words):
    predicts = self.dry_nlp.pred_next_word(start_words, model=self.model)
    words = [index2word(self.dry_nlp.token, wi) for wi in predicts]
    stress_word = WordProfile(start_words[0])
    first_word = None
    max_grade = 0
    word_i = 0
    for w in words:
      wp = WordProfile(w)
      g = 0
      if wp.syllables_count == stress_word.syllables_count:
        g += 10
      if wp.stressed_syllable_index == stress_word.stressed_syllable_index:
        g += 10
      if wp.syllables_count > 0 and stress_word.syllables_count >0 and wp.syllables[wp.syllables_count - 1].text == stress_word.syllables[stress_word.syllables_count - 1].text:
        g += 100
      g -= word_i
      word_i += 1
      # print("gen_first_word: {} {}".format(w, g))
      # for i in range(min(len(w), len(stress_word.word))):
      #   if w[-i - 1] == stress_word.word[-i -1]:
      #     g += 1
      if max_grade < g:
        max_grade = g
        first_word = w

    return first_word


  def generate_poem(self, start_line_text, lines_count=8,  branch_num=2, min_len=4, max_len=15):
    res_list = [start_line_text.split()[::-1]+['eol']]
    for i in range(lines_count - 1):
      tree_list = []
      
      start_words = res_list[-1]
      print("line {}. start_text: {}".format(i, start_words))
      fw = self.gen_first_word(start_words)
      print("first word: {}".format(fw))
      self.generate_tree(start_words+[fw], tree_list, branch_num=branch_num, min_len=min_len, max_len=max_len)

      max_grade = 0
      max_grade_line = None
      for l in tree_list:
      
        l = " ".join(l).replace(" ".join(start_words), "").split()
        grade = self.calc_grade(l, stress_word=start_words[0])
        # sent1 = " ".join(start_words[:-1][::-1])+'.'
        # sent2 = " ".join(l[:-1][::-1])+'.'
        # # odd = is_sentence_follows(sent1, sent2)
        # print("{} -> {}: {}%".format(sent1, sent2, odd))
        # grade += odd
        if grade > max_grade:
          max_grade = grade
          max_grade_line = l

      print('max_grade_line: {}'.format(max_grade_line))
      res_list.append(max_grade_line)


    return [l[::-1] for l in res_list]


  def generate_tree(self, start_words, result_list, curr_len=0, branch_num=2, min_len=4, max_len=15):
    predicts = self.dry_nlp.pred_next_word(start_words, model=self.model)
    words = [index2word(self.dry_nlp.token, wi) for wi in predicts]
    i = 0

    for w in words:
      if w == 'eol' and curr_len < min_len:
        continue

      if w == 'eol' or curr_len + 1>= max_len:
        # print("line:", " ".join(start_words+[w]) )
        if w == 'eol':
          result_list.append(start_words+[w])
        else:
          result_list.append(start_words+[w, 'eol'])

      else:
        self.generate_tree(start_words+[w], result_list, curr_len=curr_len+1, branch_num=branch_num, min_len=min_len, max_len=max_len)
      
      i += 1
      if i >= branch_num:
        break



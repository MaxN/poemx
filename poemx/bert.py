# coding: utf-8
import sys
import codecs
import numpy as np
from keras_bert import load_trained_model_from_checkpoint
from .bert_tokenization import FullTokenizer
from .hyperparams import BERT_MODEL_PATH

folder = BERT_MODEL_PATH
config_path = folder+'/bert_config.json'
checkpoint_path = folder+'/bert_model.ckpt'
vocab_path = folder+'/vocab.txt'

model = None
tokenizer = None

def init():
  global model, tokenizer
  if model is None:
    model = load_trained_model_from_checkpoint(config_path, checkpoint_path, training=True)
  if tokenizer is None:
    tokenizer = FullTokenizer(vocab_file=vocab_path, do_lower_case=False)  


def is_sentence_follows(sent1, sent2):
  init()
  tokens_sen_1 = tokenizer.tokenize(sent1)
  tokens_sen_2 = tokenizer.tokenize(sent2)

  tokens = ['[CLS]'] + tokens_sen_1 + ['[SEP]'] + tokens_sen_2 + ['[SEP]']
  
  token_input = tokenizer.convert_tokens_to_ids(tokens)      
  token_input = token_input + [0] * (512 - len(token_input))

  mask_input = [0] * 512

  seg_input = [0]*512
  len_1 = len(tokens_sen_1) + 2                   # длина первой фразы, +2 - включая начальный CLS и разделитель SEP
  for i in range(len(tokens_sen_2)+1):            # +1, т.к. включая последний SEP
      seg_input[len_1 + i] = 1  
  
  token_input = np.asarray([token_input])
  mask_input = np.asarray([mask_input])
  seg_input = np.asarray([seg_input])

  predicts = model.predict([token_input, seg_input, mask_input])[1]
  
  return int(round(predicts[0][0]*100))


if __name__ == '__main__':
  print(is_sentence_follows('Я пришел в магазин.', 'И купил молоко.'))
  print(is_sentence_follows('Я пришел в магазин.', 'Карась небо Плутон'))
  print(is_sentence_follows('Я', 'иду'))
  print(is_sentence_follows('Я', 'Я'))
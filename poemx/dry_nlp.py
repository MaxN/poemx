# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
import io
import random
import sys
import math
import os
import threading
from keras_preprocessing import text
from  collections import OrderedDict, defaultdict
import json
import re
from tensorflow.keras.models import load_model
from matplotlib import pyplot as plt
import random


def load_token(fn):
    with open(fn) as f:
        data = f.read()

        j = json.dumps(json.loads(data))
        token = text.tokenizer_from_json(j)
        token.word_counts = OrderedDict(token.word_counts)
        token.word_docs = defaultdict(int, token.word_docs)
        token.index_docs = defaultdict(int, token.index_docs)

        for (key,value) in token.word_index.items():
            if key not in token.word_docs:
                token.word_docs[key]= 0
        return token
    
def save_token(fn, token):
    with open(fn, "w") as f:
        f.write(token.to_json())
        
def hot_vect2int(v):
    return np.where(v==1)[0][0]

def index2word(token, word_index):
    for word, index in token.word_index.items():
        if index == word_index:
            return word
    return ''

def shuffle_2lines(lines1, lines2):
    if len(lines1) != len(lines2):
        raise 'len doesnt match'
    l = []
    for i in range(len(lines1)):
        l.append([lines1[i], lines2[i]])
    
    random.shuffle(l)
    ret_lines1 = []
    ret_lines2 = []
    for i in range(len(l)):
        ret_lines1.append(l[i][0])
        ret_lines2.append(l[i][1])
    return ret_lines1, ret_lines2

def text_gen(files):
    for fn in files:
        if os.path.isfile(fn):
            with open(fn, errors='backslashreplace') as f:
                text = f.read().lower()
                text = re.sub(',|\*|"|…|\.|\?|!|_|-|:', ' ', text)
                sents = re.split('\n', text)
                yield sents


def sample(preds, temperature=1.0):
    # helper function to sample an index from a probability array
    preds = np.asarray(preds).astype('float64')# always use float64
    preds = np.log(preds) / temperature
    exp_preds = np.exp(preds)
    preds = exp_preds / np.sum(exp_preds)
    probas = np.random.multinomial(1, preds, 1)
    return np.argmax(probas)

def show_token_index(token):
    return sorted(token.word_index.items(), key=lambda x: x[1])
        
class GenericDryNlp(tf.keras.callbacks.Callback):
    def __init__(self, seq_length, batch_size):
           #grapth stuff
        self.graph_x = []
        self.graph_val_x = []
        self.graph_losses = []
        self.graph_val_losses = []
        self.graph_acc = []
        self.epoch_num = 0
        self.thread_lock = threading.Lock()
        self.dtype = 'float16'
        self.batch_size = batch_size
        self.seq_length = seq_length
                
    def lock(self):
        self.thread_lock.acquire()
    def unlock(self):
        self.thread_lock.release()
        

  
    
    def print_word_dict(self):
        print('Word dict:', sorted((value,key) for (key,value) in self.token.word_index.items()))        
             
    def draw_chart(self, logs, save_to_file=None):
        self.graph_x.append(self.epoch_num)
        self.graph_losses.append(logs.get('loss'))
        self.graph_acc.append(logs.get('acc'))
        if logs.get('val_loss'): 
            self.graph_val_x.append(self.epoch_num)
            self.graph_val_losses.append(logs.get('val_loss'))
            
        
        self.epoch_num += 1
        
        if save_to_file == None and self.epoch_num % 5 == 0:
            clear_output(wait=True)
        
        plt.plot(self.graph_x, self.graph_losses, label="loss")
        plt.plot(self.graph_x, self.graph_acc, label="acc")
        if len(self.graph_val_x) > 0:
            plt.plot(self.graph_val_x, self.graph_val_losses, label="val_loss")
        plt.legend()
        if save_to_file != None:
            plt.savefig(save_to_file)
            plt.clf()
            plt.cla()
        else:
            plt.show()

    def load(self, path, model=None):
        pth = path + "/model"
        if model == None:
            model = load_model(pth + '/model.h5')
        
        model.load_weights(pth + '/model_weights.h5')
        self.input_token = drynlp.load_token(pth + "/input_token.json", )
        self.output_token = drynlp.load_token(pth + "/output_token.json")  
        self.input_gram_token = drynlp.load_token(pth + "/input_gram_token.json")  
        return model
        
class DryNlp(GenericDryNlp):
    def __init__(self, seq_length=40, words_number=50, char_level=True, batch_size=None):
        self.words_number = words_number
        self.char_level = char_level
        self.token = tf.keras.preprocessing.text.Tokenizer(num_words=words_number, char_level=char_level)
        self.prev_batch =[]
       
        GenericDryNlp.__init__(self, seq_length=seq_length, batch_size=batch_size)
        


    def get_batch_for_yield(self, batch):
        self.prev_batch = batch        
        return self.convert_text_lines_emb(batch)
    
        

        

    
    def convert_text_lines(self, lines):        
        self.token.fit_on_texts(lines)
        sequence_list = self.token.texts_to_sequences(lines)
        sequence_list = np.array(tf.keras.preprocessing.sequence.pad_sequences(sequence_list, maxlen=self.seq_length, padding='pre'))
        X, Y =  tf.keras.utils.to_categorical(sequence_list[:, :-1], num_classes=self.words_number,  dtype=self.dtype), tf.keras.utils.to_categorical(sequence_list[:, -1], num_classes=self.words_number,  dtype=self.dtype)
        return X,Y
    
    def convert_text_lines_emb(self, lines):

        self.token.fit_on_texts(lines)
        sequence_list = self.token.texts_to_sequences(lines)
        sequence_list = np.array(tf.keras.preprocessing.sequence.pad_sequences(sequence_list, maxlen=self.seq_length, padding='pre'))
        X, Y =  sequence_list[:, :-1], tf.keras.utils.to_categorical(sequence_list[:, -1], num_classes=self.words_number,  dtype=self.dtype)
        return X,Y
    

    
    def save_model(self, model, name):
        path = "{0}.model".format(name)
        if not os.path.exists(path):
            os.mkdir(path)
        
        # serialize model to JSON
        model_json = model.to_json()
        with open("{0}/model.json".format(path), "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        model.save_weights("{0}/weights.h5".format(path), overwrite=True)
        
        with open("{0}/token.json".format(path), "w") as f:
            f.write(self.token.to_json())
        
        
    def load_model(self, model, name):
        path = "{0}.model".format(name)
        # load json and create model
#         json_file = open('{0}.json'.format(name), 'r')
#         loaded_model_json = json_file.read()
#         json_file.close()
#         model = tf.keras.models.model_from_json(loaded_model_json)
        # load weights into new model
        model.load_weights("{0}/weights.h5".format(path))
        with open("{0}/token.json".format(path)) as f:
            data = f.read()
#             import pdb; pdb.set_trace()
            j = json.dumps(json.loads(data))
            self.token = text.tokenizer_from_json(j)
        self.token.word_counts = OrderedDict(self.token.word_counts)
        self.token.word_docs = defaultdict(int, self.token.word_docs)
        self.token.index_docs = defaultdict(int, self.token.index_docs)

        for (key,value) in self.token.word_index.items():
            if key not in self.token.word_docs:
                self.token.word_docs[key]= 0
        
   
    def predict(self, text, model):
        seq, _ = self.convert_text_lines_emb([text+' '])
        r = model.predict_classes(seq) 
        return self.index2word(r[0])
    def seq_for_pred(self, seq):
        if self.batch_size != None:
            while len(seq) < self.batch_size:
                seq = np.append(seq, seq, 0)
        return seq
    def generate_text_temp(self, text, model, length, temp=1.0):
        seq, _ = self.convert_text_lines_emb([text+' '])
        rtext = ''
        for i in range(length):
            seq, _ = self.convert_text_lines_emb([text+rtext+' '])
            seq = self.seq_for_pred(seq)
            preds = model.predict(seq, batch_size=self.batch_size, verbose=0)[0]
            r = self.sample(preds, temp)
            rtext = rtext + self.index2word(r)
            if not self.char_level:
                rtext += ' '
        return rtext
    
    def generate_text(self, text, model, length):
        seq, _ = self.convert_text_lines_emb([text+' '])
        rtext = ''
        for i in range(length):
            seq, _ = self.convert_text_lines_emb([text+rtext+' '])
            seq = self.seq_for_pred(seq)
            pred = model.predict_classes(seq, batch_size=self.batch_size)
            
            if self.batch_size != None:
                pred = pred[0]

            rtext = rtext + self.index2word(pred)
            if not self.char_level:
                rtext += ' '
        return rtext
    
    def on_epoch_end(self, epoch, logs):
        self.lock()
        try:
            self.draw_chart(logs)
    
            if len(self.prev_batch) == 0:
                return
            r = random.randint(0, len(self.prev_batch) - 1)
            input_str = self.prev_batch[r]
            
            print()
            print('Input:', input_str)
            print('Standart output:')
            print(self.generate_text(input_str, self.model, self.seq_length))
            for temp in [0.2, 0.5, 1.0, 1.2]:
                print('Output ({0}):'.format(temp))
                print(self.generate_text_temp(input_str, self.model, self.seq_length, temp=temp))
                print()
            print()
            self.model.reset_states()
        finally:
            self.unlock()
            
    
        
    def draw_hist(self, h):
        acc      = history.history[     'acc' ]
        val_acc  = history.history[ 'val_acc' ]
        loss     = history.history[    'loss' ]
        val_loss = history.history['val_loss' ]

        epochs   = range(len(acc)) # Get number of epochs

        #------------------------------------------------
        # Plot training and validation accuracy per epoch
        #------------------------------------------------
        plt.plot  ( epochs,     acc )
        plt.plot  ( epochs, val_acc )
        plt.title ('Training and validation accuracy')
        plt.figure()

        #------------------------------------------------
        # Plot training and validation loss per epoch
        #------------------------------------------------
        plt.plot  ( epochs,     loss )
        plt.plot  ( epochs, val_loss )
        plt.title ('Training and validation loss'   )

class DryDataSet(tf.keras.utils.Sequence):
    def __init__(self, fn, nlp, steps=100, batch_size=128, shuffle=True):
        self.fn = fn
        self.nlp = nlp
        self.batch_size = batch_size
        self.length = os.path.getsize(self.fn)
        self.f = None
        self.open_file()
        self.buf_size = 10000
        self.steps = steps
        self.batch = []
        self.buf = ''
        self.shuffle = shuffle
        
    def __len__(self):
        return self.steps
        return math.ceil(self.length / self.seq_length / self.batch_size)
    
    def open_file(self):
#         print("open file:" + self.fn)
        if (self.f != None):
            self.f.close()
        self.f = io.open(self.fn, errors='backslashreplace') 
        
    def read_file(self):
        if len(self.batch) > self.batch_size:
            return
        self.buf = self.f.read(self.buf_size)
        if self.buf == '':
#             print("EOF!!!!!!!!!!!!")
            self.open_file()
            
        
        for i in range(0, len(self.buf) - self.nlp.seq_length, 3):
            self.batch.append(self.buf[i:i+self.nlp.seq_length].lower())
        self.read_file()
        if self.shuffle:
            random.shuffle(self.batch)
        
    def read_file_lines(self):
        if len(self.batch) > self.batch_size:
            return
        
        for l in self.f.readline():
            pass
            
        
    def __getitem__(self, idx):
        self.nlp.lock()
        try:
            self.read_file()
            new_batch = self.batch[:self.batch_size]
            del self.batch[:self.batch_size]
        
            return self.nlp.get_batch_for_yield(new_batch)
        finally:
            self.nlp.unlock()
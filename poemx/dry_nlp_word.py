# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import numpy as np
import io
import random
import sys
import math
import os
import threading
from tensorflow.keras.models import load_model

from poemx.dry_nlp  import GenericDryNlp, hot_vect2int, index2word, sample, text_gen, save_token, load_token

from poemx.udpipe_encoder import UdPipeEncoder
from poemx.hyperparams import UDPIPE_FILE
from .telegram_notifier import TelegramNotifier
from .attention_layer import AttentionWeightedAverage

class DryNlpWord(GenericDryNlp):
    def __init__(self, model_output, seq_length=10, words_num=30000, batch_size=None):
        self.words_num = words_num
        self.token = tf.keras.preprocessing.text.Tokenizer(num_words=words_num)
        self.model_output = model_output
        self.bot = TelegramNotifier(token=os.environ['DS_NOTIFIER_BOT'], chat_id=os.environ['DS_NOTIFIER_CHAR_ID'])
        GenericDryNlp.__init__(self, seq_length=seq_length, batch_size=batch_size)

    
    def convert_text_lines_emb(self, text):
        word_seq_list = self.token.texts_to_sequences(text)
        word_seq_list = np.array(tf.keras.preprocessing.sequence.pad_sequences(word_seq_list, maxlen=self.seq_length, padding='pre',truncating='pre'))            
        
        X = word_seq_list[:, :-1]
        Y = word_seq_list[:, -1:]
        return X, Y
    
    def pred_next_word(self, words, model):
        pred_word_list = self.predict_list(' '.join(words), model)
        return pred_word_list.argsort()[::-1][:300] # get first 3

    def predict_list(self, text, model):
        r = self.convert_text_lines_emb([text+' eol'])
        r = model.predict(r[0]) 
        return r[0]

    def generate_text(self, start_text, length, model):
        ret_line = start_text.split()
        
        while len(ret_line) < length: 
          pred_list = self.pred_next_word(ret_line, model)
          wi = pred_list[0]
          ret_line.append(index2word(self.token, wi))
        return ' '.join(ret_line[::-1])
        
    def save(self, pth, model):
        if not os.path.exists(pth):
            os.makedirs(pth)
        model.save(pth + "/model.h5", overwrite=True)
        model.save_weights(pth + "/model_weights.h5", overwrite=True)
        save_token(pth + "/token.json", self.token)
        
        
    def load(self, pth, model=None):
        if model == None:
            model = load_model(pth + '/model.h5')
        
        model.load_weights(pth + '/model_weights.h5')
        #self.token = load_token(pth + "/token.json")
        return model
            
        
    def on_epoch_end(self, epoch, logs):
        self.lock()
        try:
        
            self.save(self.model_output+'/epoch', self.model)
            chart_fn = self.model_output+'/epoch/chart.png'
            self.draw_chart(logs, save_to_file=chart_fn)
            print()
            # import pdb; pdb.set_trace()
            gen_text = self.generate_text("медяки метро в считают как eol", length=40, model=self.model)
            
            self.bot.send_msg(
                "{}: loss={}, acc={}, val_loss={}, val_acc={}".format(
                    epoch, 
                    logs.get('loss'),
                    logs.get('acc'),
                    logs.get('val_loss'),
                    logs.get('val_acc')
                    ))
            self.bot.send_msg("generated text: {}".format(gen_text))
            self.bot.send_img(chart_fn)
            print(gen_text)
            print()

        except Exception as err:
            print(err)    
        finally:
            self.unlock()
        
            
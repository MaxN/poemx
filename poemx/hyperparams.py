from pathlib import Path

SEQ_LENGTH = 10 #max length of word
UDPIPE_FILE = 'poemx/models/russian-syntagrus-ud-2.3-181115.udpipe'
BERT_MODEL_PATH = str(Path(__file__).parent.joinpath('models').joinpath('multi_cased_L-12_H-768_A-12').resolve())
# MODEL_OUTPUT_PATH = Path(__file__).parent.joinpath('model').resolve()

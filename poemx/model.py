import tensorflow as tf
from .attention_layer import AttentionWeightedAverage

def get_model(seq_length, words_num):
  word_input = tf.keras.layers.Input(shape=(seq_length,), name = 'main_input')
  emb_x = tf.keras.layers.Embedding(words_num, 168)(word_input)
  lstm_x1 = (tf.keras.layers.LSTM(300, return_sequences=True, dropout=0.2, recurrent_dropout=0.2))(emb_x)
  x = tf.keras.layers.SpatialDropout1D(0.2)(lstm_x1)
  x = (tf.keras.layers.LSTM(300, return_sequences=True, dropout=0.2, recurrent_dropout=0.2))(x)
  # print('emb_x', emb_x.shape)
  # print('lstm_x1', lstm_x1.shape)
  # print('x', x.shape)
  x = tf.keras.layers.concatenate([emb_x, lstm_x1, x])
  x = AttentionWeightedAverage()(x)
  x = tf.keras.layers.Flatten()(x)
  # x = tf.keras.layers.Dense(256, activation='relu')(x)
  main_output = tf.keras.layers.Dense(words_num, activation='softmax', name='main_output')(x)
  model = tf.keras.models.Model(inputs=[word_input], outputs=[main_output])

  # model = multi_gpu_model(model, gpus=2)
  #model.compile(loss='sparse_categorical_crossentropy', optimizer=tf.keras.optimizers.RMSprop(lr=0.001), metrics=['accuracy'])
  model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
  # model.compile(loss='categorical_crossentropy', optimizer=SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True), metrics=['accuracy'])
  return model


import telegram
import os

class TelegramNotifier:
  def __init__(self, token, chat_id):
    self.chat_id = chat_id
    self.bot = telegram.Bot(token=token)

  def send_msg(self, msg):
    self.bot.send_message(chat_id=self.chat_id, text=msg)

  def send_img(self, path_to_img):
    self.bot.send_photo(chat_id=self.chat_id, photo=open(path_to_img, 'rb'))

  def get_chat_id(self):
    upd = self.bot.get_updates()
    if len(upd) > 0:
      return upd[-1].message.chat_id
    else:
      None

  def get_updates(self):
    upd = self.bot.get_updates()
    for u in upd:
      print(u)

if __name__ == '__main__':
  bot = TelegramNotifier(token=os.environ['DS_NOTIFIER_BOT'], chat_id=os.environ['DS_NOTIFIER_CHAR_ID'])
  print(bot.get_chat_id())
  bot.send_msg('hello')
  bot.send_img('/home/snake/Pictures/current.png')
from ufal.udpipe import Model, Pipeline, ProcessingError

class UdPipeEncoder:
    def __init__(self, filename):
        self.model = Model.load(filename)
        self.pipeline = Pipeline(self.model, 'tokenize', Pipeline.DEFAULT, Pipeline.DEFAULT, 'conllu')
        self.ignore_pos = ['PUNCT']
        
    def process_gram_tag(self, gram: str):
        gram = gram.strip().split("|")
        dropped = ["Animacy", "Aspect", "NumType"]
        gram = [grammem for grammem in gram if sum([drop in grammem for drop in dropped]) == 0]
        return "|".join(sorted(gram)) if gram else "_"
    
    def encode(self, texts, with_features=False):
        rlist = []
        for t in texts:
            r = self.pipeline.process(t.lower())
            words = r.split('\n')
            
            line = []
            for w in words:
#                 import pdb; pdb.set_trace()
                if len(w) == 0 or w[0] == '#':
                    continue
                
                pos = w.split("\t")
                if pos[3] in  self.ignore_pos:
                    continue
                    
                if not with_features:
                    line.append(pos[2] + '_' + pos[3])
                else:
                    line.append(pos[2] + '_' + pos[3] +'#'+ self.process_gram_tag(pos[5]))
                
            rlist.append(line)
        return rlist
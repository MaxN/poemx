import io
import random
import sys
import math
import os
import re
import glob
import datetime as dt
from poemx.hyperparams import SEQ_LENGTH, UDPIPE_FILE
from poemx.dry_nlp import text_gen
import json

def prepare_corpus(seq_length, shuffle=True):
    files =  [f for f in glob.glob("corpus/*.txt", recursive=True)]
    

    if not os.path.exists('tmp/dataset'):
        os.makedirs('tmp/dataset')
    
    buf_words = []
    
    with open('tmp/dataset/all_train.txt', 'w') as f_train:
        with open('tmp/dataset/all_valid.txt', 'w') as f_valid:
            for text in text_gen(files):
                for l in text:
                    l = l.strip()
                    if l == '':
                        continue
                    l = l + ' eol'
                   
                    
                    words  = re.split('\W+', l)
                    words = [w  for w in words if w != '']
                    
                    words = words[::-1]
                    
                    buf_words = buf_words + words

                    while len(buf_words) > seq_length:
                        for i in range(seq_length - 2, seq_length + 1):
                            
                            words = buf_words[:i]
                            
                    
                            line_output = " ".join(words)   
                            if random.randint(0,100) >= 70:
                                f_valid.write(line_output + '\n')                
                            else:
                                f_train.write(line_output + '\n')                
                        del buf_words[0]
                        
def from_json():
    with open('corpus/all_django.json') as f:
        with open('corpus/all.txt', 'w') as f_out:
            j = json.load(f)
            for poem in j:
                txt = poem['fields']['text']
                txt.replace('\\n', '\n')
                f_out.write(txt)
                f_out.write('\n')
                f_out.write('\n')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        prepare_corpus(seq_length=SEQ_LENGTH)
    elif sys.argv[1] == 'json':
        from_json()
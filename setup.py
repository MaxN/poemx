import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="poemx",
    version="0.0.1",
    author="Max Nedelchev",
    author_email="max.nedelchev@gmail.com",
    description="Poemx: library for generating poems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/MaxN/poemx",
    packages=setuptools.find_packages(),
    include_package_data=True,  
    package_data={
        'poemx': ['model/*']
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'rhymex'
    ]
)
# -*- coding: utf-8 -*-
import unittest

from poemx import Poemx


class TestMain(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.model = Poemx(model_path='model')
        pass

    def test_stress(self):
        seed_words = ['майка',
          'соломка',
          'изжить',
          'виться',
          'данный',
          'зорька',
          'банка',
          'оттечь',
          'пора',
          'меда',
          'советского',
          'союза',
          'изжила',
          'автоподъёмник',
          'каракуля',
          'супервайзер',
          'колесом',
          'братишь']

        text = self.model.generate_poem(seed_words=['город', 'жизнь'], lines_count=10)
        for l in text:
          print(' '.join(l))
        pass

if __name__ == '__main__':
    unittest.main()
import io
import random
import sys
import math
import os
import re
import glob
import datetime as dt
import threading
import tensorflow as tf

from poemx.dry_nlp  import hot_vect2int, index2word, shuffle_2lines, show_token_index, load_token
from poemx.dry_nlp_word import DryNlpWord
from poemx.model import get_model
from poemx.hyperparams import SEQ_LENGTH
import random


def fit_gen(fn, nlp, batch_size = 128,  debug=False, endless=True):    
    while True:
        raw_text = []
        token_text = []
        with open(fn, errors='backslashreplace') as f:
            while True:
                
                raw_line = f.readline()
                token_line = f.readline().strip()
                if raw_line == '':
                    break

                raw_text.append(raw_line)
                token_text.append(token_line.split(' '))


                if len(raw_text) >= batch_size:
                    if debug:
                        yield nlp.text2input(token_text, raw_text), raw_text
                    else:
                        yield nlp.text2input(token_text, raw_text)
                    raw_text = []
                    token_text = []
        if not endless:
            break
def blocks(files, size=65536):
    while True:
        b = files.read(size)
        if not b: break
        yield b


class DryDataSet(tf.keras.utils.Sequence):
    def __init__(self, fn, nlp, batch_size, debug=False):
        self.fn=fn
        self.nlp = nlp
        self.lock = threading.Lock()
        self.f = None
        self.batch_size = batch_size
        self.calc_len()
        self.open_file()
        self.raw_text = []
        self.token_text = []
        self.debug = debug
        
    def calc_len(self):
        with open(self.fn, "r",encoding="utf-8",errors='ignore') as f:
            self.len = sum(bl.count("\n") for bl in blocks(f))
        self.len = int(self.len / 2 / self.batch_size)

    def open_file(self):
        if (self.f != None):
            self.f.close()
        self.f = open(self.fn, errors='backslashreplace') 

    def read_lines(self):
        if len(self.raw_text) < self.batch_size:
            while len(self.raw_text) < self.batch_size*10:
                raw_line = self.f.readline()
                token_line = self.f.readline().strip()

                if len(raw_line) == 0 or len(token_line) == 0:
                    self.open_file()

                self.raw_text.append(raw_line)
                self.token_text.append(token_line.split(' '))

        ret_text = self.raw_text[0:self.batch_size]
        ret_token = self.token_text[0:self.batch_size]
        del self.raw_text[0:self.batch_size]
        del self.token_text[0:self.batch_size]
        
        return ret_text, ret_token

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
       
        with self.lock:
            raw_text, token_text = self.read_lines()

            raw_text, token_text = shuffle_2lines(raw_text, token_text)
            if len(raw_text) >= self.batch_size:
                if self.debug:
                    return nlp.text2input(token_text, raw_text), raw_text
                else:
                    return nlp.text2input(token_text, raw_text)
    
def test_ds(nlp):
    debug_ds = DryDataSet('tmp/dataset/all_train.txt', nlp, batch_size = 128, debug=True)
    X_Y, raw_text = debug_ds.__getitem__(0)
    X_main = X_Y[0]['main_input']
    X_gram = X_Y[0]['gram_input']
    Y = X_Y[1]['main_output']
    print('=======================')
    print('X_main', X_main.shape)
    print('X_gram', X_gram.shape)
    print('Y', Y.shape)

    i = random.randint(0, X_main.shape[0])
    print('raw_text: ', raw_text[i])
    for j in range(SEQ_LENGTH - 1):
        print("X_main = {}, X_gram= {}, Word={}, Gram={}".format(
            X_main[i][j],
            (X_gram[i][j]),
            index2word(nlp.input_token, X_main[i][j]),
            index2word(nlp.input_gram_token, (X_gram[i][j]))
            ))    
    print("Y = {}, Y_Word={}, ".format(
            hot_vect2int(Y[i]),
            index2word(nlp.output_token, hot_vect2int(Y[i])),
            ))    
    print('=======================')



if __name__ == '__main__':

    nlp = DryNlpWord('tmp', output_words_number=50000, input_words_number=25000, input_gram_number=300)
    test_ds(nlp)

    # model = get_model(seq_length=SEQ_LENGTH-1, input_words_num=nlp.output_words_number, gram_input_num=nlp.input_gram_number, output_words_num=nlp.output_words_number)
    model = nlp.load('tmp/epoch')
    nlp.input_token = load_token("tmp/input_token.json", )
    nlp.output_token = load_token("tmp/output_token.json")  
    nlp.input_gram_token = load_token("tmp/input_gram_token.json")  
    print('input_token')
    print(show_token_index(nlp.input_token)[-10:])
    print('input_gram_token')
    print(show_token_index(nlp.input_gram_token)[-10:])
    print('output_token')
    print(show_token_index(nlp.output_token)[-10:])
    # import pdb; pdb.set_trace()
    model.summary()
    bs = 64*3    
    
    train_ds = DryDataSet('tmp/dataset/all_train.txt', nlp, batch_size = bs)
    valid_ds = DryDataSet('tmp/dataset/all_valid.txt', nlp, batch_size = bs)
    h = model.fit_generator(
        train_ds,
        # fit_gen('tmp/dataset/all_train.txt', nlp, batch_size = bs), 
        # steps_per_epoch=int(train_len / bs), 
        epochs=1000, 
        validation_data = valid_ds,
        # validation_data=fit_gen('tmp/dataset/all_valid.txt', nlp, batch_size = bs), 
        # validation_steps=int(valid_len / bs), 
        verbose=1, 
        max_queue_size=100,
        workers=1,
        validation_freq=5,
        # use_multiprocessing=True,
        callbacks=[
            nlp,
    #        tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=5)

            ])

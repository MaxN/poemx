import io
import random
import sys
import math
import os
import re
import glob
import datetime as dt
import threading
import tensorflow as tf

from poemx.dry_nlp  import hot_vect2int, index2word, shuffle_2lines, show_token_index, save_token, load_token
from poemx.dry_nlp_word import DryNlpWord
from poemx.model import get_model
from poemx.hyperparams import SEQ_LENGTH

def read_corpus_file(fn):
  with open(fn) as f:
    corpus = f.readlines()#[:1000]
    return corpus

def gen(corpus, nlp, batch_size):
  batch = []
  while True:
    for l in corpus:
      if len(batch) >= batch_size:
        random.shuffle(batch)
        yield nlp.convert_text_lines_emb(batch)
        batch =[]
      batch.append(l)



def test(X, Y, text, token):
    print('=======================')
    print('X shape', X.shape)
    
    print('Y shape', Y.shape)

    i = random.randint(0, X.shape[0])
    print('text: ', text[i])
    for j in range(SEQ_LENGTH - 1):
        print("Xi = {}, word={}".format(
            X[i][j],
            index2word(token, X[i][j]),
            ))    
    print("Yi = {}, word={}".format(
            Y[i][0],
            index2word(token, Y[i][0]),
            ))    
    print('=======================')

if __name__ == '__main__':
  nlp = DryNlpWord(model_output='tmp', seq_length=SEQ_LENGTH)
  corpus = read_corpus_file('tmp/dataset/all_train.txt')
  val_corpus = read_corpus_file('tmp/dataset/all_valid.txt')
  
  if not os.path.exists('tmp/token.json'):
    nlp.token.fit_on_texts(corpus)
    save_token('tmp/token.json', nlp.token)
  else:
    nlp.token = load_token('tmp/token.json')
  bs = 128*2
  
  model = get_model(seq_length=SEQ_LENGTH - 1, words_num=nlp.words_num)
  model = nlp.load('tmp/epoch', model)
  model.summary()

  X, Y = nlp.convert_text_lines_emb(corpus)
  val_X, val_Y = nlp.convert_text_lines_emb(val_corpus)
 # for i in range(10):
#    test(X, Y, corpus, nlp.token)
  #halt()
  model.fit(X,Y,
    epochs=4000, 
    verbose=1, 
    max_queue_size=100,
    workers=1,
    validation_data=(val_X, val_Y),
    batch_size=bs,
    shuffle=True,
    validation_freq=5,
    callbacks=[
        nlp
        ])

  exit()
  model.fit_generator(
    gen(corpus, nlp, batch_size=bs), 
    epochs=1000, 
    verbose=1, 
    max_queue_size=100,
    workers=1,
#    validation_split=0.2,
    steps_per_epoch=int(len(corpus) / bs), 
    validation_data=gen(val_corpus, nlp, batch_size = bs), 
    validation_steps=int(len(val_corpus) / bs), 
    validation_freq=5,
    callbacks=[
        nlp,
        # tf.keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=10)

        ])



